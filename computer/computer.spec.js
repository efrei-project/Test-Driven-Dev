import {Computer} from "./computer.js";

describe("Computer Class Test", () => {
  describe("- Sum method", () => {
    describe("- 2 params", () => {
      test("2 params: 1 and 1, expect 2", () => {
        expect(Computer.sum(1, 1)).toEqual(2);
      });
      test("2 params: 1 and 2, expect 3", () => {
        expect(Computer.sum(1, 2)).toEqual(3);
      });
      test("2 params: 1 and 3, expect 4", () => {
        expect(Computer.sum(1, 3)).toEqual(4);
      });
      describe("- Signed and float", () => {
        test("2 params: -1 and 3, expect 2", () => {
          expect(Computer.sum(-1, 3)).toEqual(2);
        });
        test("2 params: 1,5 and 3, expect 4,5", () => {
          expect(Computer.sum(1.5, 3)).toEqual(4.5);
        });
      })
    })
    describe("- 3 params", () => {
      test("3 params: 1 and 2 and 3, expect 6", () => {
        expect(Computer.sum(1, 2, 3)).toEqual(6);
      });
      test("3 params: 1 and 2 and 4, expect 7", () => {
        expect(Computer.sum(1, 2, 4)).toEqual(7);
      });
      test("3 params: 1 and 2 and 5, expect 8", () => {
        expect(Computer.sum(1, 2, 5)).toEqual(8);
      });
      describe("- Signed and float", () => {
        test("3 params: -1 and 3 and 5, expect 2", () => {
          expect(Computer.sum(-1, 3, 5)).toEqual(7);
        });
        test("3 params: 1,5 and 3 and 5, expect 4,5", () => {
          expect(Computer.sum(1.5, 3, 5)).toEqual(9.5);
        });
      })
    })
    describe("- 5 params", () => {
      test("5 params: 1, 2, 3, 4 and 5 expect 15", () => {
        expect(Computer.sum(1, 2, 3, 4, 5)).toEqual(15);
      });
      test("5 params: 1, 2, 3, 4 and 6 expect 16", () => {
        expect(Computer.sum(1, 2, 3, 4, 6)).toEqual(16);
      });
      test("5 params: 1, 2, 3, 4 and 7 expect 17", () => {
        expect(Computer.sum(1, 2, 3, 4, 7)).toEqual(17);
      });
      describe("- Signed and float", () => {
        test("5 params: -1, 2, 3, 4 and 5 expect 13", () => {
          expect(Computer.sum(-1, 2, 3, 4, 5)).toEqual(13);
        });
        test("5 params: 1,5, 2, 3, 4 and 5 expect 15.5", () => {
          expect(Computer.sum(1.5, 2, 3, 4, 5)).toEqual(15.5);
        });
      })
    })
    describe("- Error management", () => {
      test("0 param", () => {
        expect(() => {
          Computer.sum().toThrow(new Error("function need 2 or more args"));
        })
      });
      test("1 params", () => {
        expect(() => {
          Computer.sum(854).toThrow(new Error("function need 2 or more args"))
        });
      });
      test("boolean param", () => {
        expect(() => {
          Computer.sum(true).toThrow(new Error("Params is not a number"))
        });
      });
      test("string param", () => {
        expect(() => {
          Computer.sum("hello").toThrow(new Error("Params is not a number"))
        });
      });
    })
  });
  describe("- Sub method", () => {
    describe("- 2 params", () => {
      test("2 params: 1 and 1, expect 0", () => {
        expect(Computer.sub(1, 1)).toEqual(0);
      });
      test("2 params: 1 and 2, expect -1", () => {
        expect(Computer.sub(1, 2)).toEqual(-1);
      });
      test("2 params: 1 and 3, expect -2", () => {
        expect(Computer.sub(1, 3)).toEqual(-2);
      });
      describe("- Signed and float", () => {
        test("2 params: -1 and 3, expect -4", () => {
          expect(Computer.sub(-1, 3)).toEqual(-4);
        });
        test("2 params: 1,5 and 3, expect -1.5", () => {
          expect(Computer.sub(1.5, 3)).toEqual(-1.5);
        });
      })
    })
    describe("- 3 params", () => {
      test("3 params: 1 and 2 and 3, expect -4", () => {
        expect(Computer.sub(1, 2, 3)).toEqual(-4);
      });
      test("3 params: 1 and 2 and 4, expect -5", () => {
        expect(Computer.sub(1, 2, 4)).toEqual(-5);
      });
      test("3 params: 1 and 2 and 5, expect -6", () => {
        expect(Computer.sub(1, 2, 5)).toEqual(-6);
      });
      describe("- Signed and float", () => {
        test("3 params: -1 and 3 and 5, expect -9", () => {
          expect(Computer.sub(-1, 3, 5)).toEqual(-9);
        });
        test("3 params: 1,5 and 3 and 5, expect -6.5", () => {
          expect(Computer.sub(1.5, 3, 5)).toEqual(-6.5);
        });
      })
    })
    describe("- 5 params", () => {
      test("5 params: 1, 2, 3, 4 and 5 expect -13", () => {
        expect(Computer.sub(1, 2, 3, 4, 5)).toEqual(-13);
      });
      test("5 params: 1, 2, 3, 4 and 6 expect -14", () => {
        expect(Computer.sub(1, 2, 3, 4, 6)).toEqual(-14);
      });
      test("5 params: 1, 2, 3, 4 and 7 expect -15", () => {
        expect(Computer.sub(1, 2, 3, 4, 7)).toEqual(-15);
      });
      describe("- Signed and float", () => {
        test("5 params: -1, 2, 3, 4 and 5 expect -15", () => {
          expect(Computer.sub(-1, 2, 3, 4, 5)).toEqual(-15);
        });
        test("5 params: 1,5, 2, 3, 4 and 5 expect -13.5", () => {
          expect(Computer.sub(1.5, 2, 3, 4, 5)).toEqual(-12.5);
        });
      })
    })
    describe("- Error management", () => {
      test("0 param", () => {
        expect(() => {
          Computer.sub().toThrow(new Error("function need 2 or more args"));
        })
      });
      test("1 params", () => {
        expect(() => {
          Computer.sub(854).toThrow(new Error("function need 2 or more args"))
        });
      });
      test("boolean param", () => {
        expect(() => {
          Computer.sub(true).toThrow(new Error("Params is not a number"))
        });
      });
      test("string param", () => {
        expect(() => {
          Computer.sub("hello").toThrow(new Error("Params is not a number"))
        });
      });
    })
  });
  describe("- Multiple method", () => {
    describe("- 2 params", () => {
      test("2 params: 1 and 1, expect ", () => {
        expect(Computer.multi(1, 1)).toEqual(1);
      });
      test("2 params: 1 and 2, expect 2", () => {
        expect(Computer.multi(1, 2)).toEqual(2);
      });
      test("2 params: 2 and 3, expect 6", () => {
        expect(Computer.multi(2, 3)).toEqual(6);
      });
      describe("- Signed and float", () => {
        test("2 params: -1 and 3, expect -3", () => {
          expect(Computer.multi(-1, 3)).toEqual(-3);
        });
        test("2 params: 1,5 and 4, expect 6", () => {
          expect(Computer.multi(1.5, 4)).toEqual(6);
        });
      })
    })
    describe("- 3 params", () => {
      test("3 params: 1 and 2 and 3, expect 6", () => {
        expect(Computer.multi(1, 2, 3)).toEqual(6);
      });
      test("3 params: 1 and 2 and 4, expect 8", () => {
        expect(Computer.multi(1, 2, 4)).toEqual(8);
      });
      test("3 params: 1 and 2 and 5, expect 10", () => {
        expect(Computer.multi(1, 2, 5)).toEqual(10);
      });
      describe("- Signed and float", () => {
        test("3 params: -1 and 3 and 5, expect -15", () => {
          expect(Computer.multi(-1, 3, 5)).toEqual(-15);
        });
        test("3 params: 1,5 and 4 and 5, expect 30", () => {
          expect(Computer.multi(1.5, 4, 5)).toEqual(30);
        });
      })
    })
    describe("- 5 params", () => {
      test("5 params: 1, 2, 3, 4 and 5 expect 120", () => {
        expect(Computer.multi(1, 2, 3, 4, 5)).toEqual(120);
      });
      test("5 params: 1, 2, 3, 4 and 6 expect 144", () => {
        expect(Computer.multi(1, 2, 3, 4, 6)).toEqual(144);
      });
      test("5 params: 1, 2, 3, 4 and 7 expect 168", () => {
        expect(Computer.multi(1, 2, 3, 4, 7)).toEqual(168);
      });
      describe("- Signed and float", () => {
        test("5 params: -1, 2, 3, 4 and 5 expect -120", () => {
          expect(Computer.multi(-1, 2, 3, 4, 5)).toEqual(-120);
        });
        test("5 params: 1,5, 2, 3, 4 and 5 expect 180", () => {
          expect(Computer.multi(1.5, 2, 3, 4, 5)).toEqual(180);
        });
      })
    })
    describe("- Error management", () => {
      test("0 param", () => {
        expect(() => {
          Computer.multi().toThrow(new Error("function need 2 or more args"));
        })
      });
      test("1 params", () => {
        expect(() => {
          Computer.multi(854).toThrow(new Error("function need 2 or more args"))
        });
      });
      test("boolean param", () => {
        expect(() => {
          Computer.multi(true).toThrow(new Error("Params is not a number"))
        });
      });
      test("string param", () => {
        expect(() => {
          Computer.multi("hello").toThrow(new Error("Params is not a number"))
        });
      });
    })
  });
  xdescribe("Computer Class - division method", () => {
    //test div par zero
    // 2-3 tests with 2 param
    // test with 3 param
  });
})
