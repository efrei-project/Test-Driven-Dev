export class Computer{
  constructor(){

  }
  static sum(...params){
    //test(params)
    let number = 0
    params.forEach(param => {
      number += param;
    });
    return number;
  }
  static sub(...params){
    //test(params)
    let number = params[0];
    for(let i = 1; i < params.length; i++){
      number -= params[i];
    }
    return number;
  }

  static multi(...params){
    //test(params);
    let number = 1;
    params.forEach(param => {
      number = number * param;
    });
    return number;
  }

/*
  test(...params){
    if (params.length < 2){
      throw new Error("function need 2 or more args")
    }
    params.forEach(param =>{
      if(typeof param != "number"){
        throw new Error("Param is not a number");
      };
    });
  }*/
}
