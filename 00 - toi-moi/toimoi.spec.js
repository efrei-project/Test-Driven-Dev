import { toimoi } from "./toi-moi"

describe("ToiMoi function", () => {
  test("Param = empty, except Un pour toi, Un pour moi", () => {
    expect(toimoi("")).toEqual("Un pour toi, Un pour moi");
  })
  test("Param = louis, except Un pour louis, Un pour moi", () => {
    expect(toimoi("louis")).toEqual("Un pour louis, Un pour moi");
  })
  test("Param = Ben, except Un pour Ben, Un pour moi", () => {
    expect(toimoi("Ben")).toEqual("Un pour Ben, Un pour moi");
  })
  test("Param = Cristiano, except Meilleur joueur du monde", () => {
    expect(toimoi("Cristiano")).toEqual("Meilleur joueur du monde");
  })
  test("Param = true, except Un pour toi, Un pour moi", () => {
    expect(toimoi(true)).toEqual("Un pour toi, Un pour moi");
  })
  test("Param = 152, except Un pour toi, Un pour moi", () => {
    expect(toimoi(152)).toEqual("Un pour toi, Un pour moi");
  })

})
