export const hamming = (string1, string2) => {
  let HammingNumber = 0;
  if (string1.length != string2.length){
    throw new Error("Sequence should be same size");
  };
  if(!check(string1) || !check(string2)){
    throw new Error("Letters should be A, T, C or G");
  };

  for(let i = 0; i < string1.length; i++){
    if (string1.charAt(i) != string2.charAt(i)){
      HammingNumber++;
    }
  }
  return  HammingNumber;
}

function check(string){
  for(let i = 0; i < string.length; i++){
    if(string.charAt(i) != "A" && string.charAt(i) != "T" && string.charAt(i) != "G" && string.charAt(i) != "C"){
      return false;
    };
  };
  return true;
}
