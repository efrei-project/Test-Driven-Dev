import { pikachu } from "./pikachu"

describe("Pika Pikaaaaaaa Pikaaaachuuuuuu", ()=>{

  test('number is a multiple of 3', () =>{
    expect(pikachu(33)).toEqual("Pika");
  });
  test('number is a multiple of 5', () =>{
    expect(pikachu(55)).toEqual("Chu");
  });
  test('number is a multiple of 3 and 5', () =>{
    expect(pikachu(15)).toEqual("PikaChu");
  });
  test('number is not multiple of 3 or 5', () =>{
    expect(pikachu(29)).toEqual(29);
  });
  test('number is 0', () =>{
    expect(pikachu(0)).toEqual("PikaChu");
  });

  describe("Exepecting error", () =>{
    test("return error when passed boolean as param", () =>{
      expect(() =>{
        pikachu(true).toThrow(Error("pdfdsfhfhgsfm is not a number"))
      });
    });

    test("return error when passed string as param", () =>{
      expect(() =>{
        pikachu("hello").toThrow(Error("param is not a number"))
      });
    })
  });


})
