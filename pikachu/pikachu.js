export const pikachu = function(number) {
  if ((typeof number === "string") || (typeof number === "boolean")){
    throw new TypeError("param is not a number")
  } else {
    let multipleThree = number % 3 === 0;
    let multipleFive = number % 5 === 0;
    if (multipleThree){
      if(multipleFive){
        return "PikaChu";
      } else {
        return "Pika";
      }
    } else {
      if(multipleFive){
        return "Chu"
      }
      else {
        return number
      }
    }
  }
};
