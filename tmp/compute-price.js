export const computePrice = (orders) => {

  if(orders.length > 5){
    throw Error("Too many orders!!!@@");
  }

  // if orders contain one order
  if(orders.length == undefined){
    return ordering(orders);
  }
  // if orders contain multiple order
  else {
    let whatYouPay = 0;
    orders.forEach(order => {
      whatYouPay += ordering(order);
    });
    return whatYouPay;
  }
}

//function to return price X quantity for one order
function ordering(order){
  if(order.price == undefined || order.quantity == undefined){
    throw Error("Invalid order");
  }
  if(typeof order.price != "number" || typeof order.quantity != "number"){
    throw Error("Incorrect type");
  }
  if(order.price < 0 || order.quantity < 0){
    throw Error("Negative values are forbidden");
  }
  if(order.price == 0 || order.quantity == 0){
    throw Error("Zero values are forbidden.");
  }
  return order.price * order.quantity;
}
