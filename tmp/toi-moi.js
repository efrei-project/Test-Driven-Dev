export const toimoi = (name) => {
  name = (name == "" || typeof name != "string") ? "toi" : name
  return (name == "Cristiano") ? "Meilleur joueur du monde" : `Un pour ${name}, Un pour moi`
}
