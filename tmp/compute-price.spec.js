import { computePrice } from "./compute-price";

describe("computePrice function", () => {
  test("Param: 1 order expect 2", () => {
    let order = {
                  price: 1,
                  quantity: 2,
                }
    expect(computePrice(order)).toEqual(2);
  });
  test("Param: 2 order expect 7,5", () => {
    let orders =  [
                    {
                      price: 1,
                      quantity: 2
                    },
                    {
                      price: 5.5,
                      quantity: 1
                    }
                  ]
    expect(computePrice(orders)).toEqual(7.5);
  });
  test("Param: orders = 6 orders expect Error", () => {
    let orders =  [
                    {
                      price: 1,
                      quantity: 2
                    },
                    {
                      price: 1,
                      quantity: 2
                    },
                    {
                      price: 1,
                      quantity: 2
                    },
                    {
                      price: 1,
                      quantity: 2
                    },
                    {
                      price: 1,
                      quantity: 2
                    },
                    {
                      price: 5.5,
                      quantity: 1
                    }
                  ]
    expect(() => {
      computePrice(orders).toThrow(Error("Too many orders!!!@@"));
    });
  });
  test("Param: bad field expect Error", () => {
    let orders =  [
                    {
                      whatthisthing: 1,
                      quantity: 2
                    },
                    {
                      price: 1,
                      quantity: 2
                    }
                  ]
    expect(() => {
      computePrice(orders).toThrow(Error("Invalid order"));
    });
  });
  test("Param: negative value expect Error", () => {
    let orders =  [
                    {
                      price: -1,
                      quantity: 2
                    },
                    {
                      price: 1,
                      quantity: 2
                    }
                  ]
    expect(() => {
      computePrice(orders).toThrow(Error("Negative values are forbidden"));
    });
  });
  test("Param: zero value expect Error", () => {
    let orders =  [
                    {
                      price: 0,
                      quantity: 2
                    },
                    {
                      price: 1,
                      quantity: 2
                    }
                  ]
    expect(() => {
      computePrice(orders).toThrow(Error("Zero values are forbidden."));
    });
  });
  test("Param: bad type value expect Error", () => {
    let orders =  [
                    {
                      price: true,
                      quantity: 2
                    },
                    {
                      price: 1,
                      quantity: 2
                    }
                  ]
    expect(() => {
      computePrice(orders).toThrow(Error("Incorrect type"));
    });
  });
})
