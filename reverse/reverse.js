export function reverseString(string) {
  if(typeof string === "string"){
    let reversedString = "";
    for (let i = string.length; i > 0; i--)
      reversedString += string[i - 1];

    return reversedString
  } else {
    throw new TypeError("Param is not a string");
  }
}
