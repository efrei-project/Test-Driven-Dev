import { reverseString } from "./reverse"

describe("Reverse String", () =>{
  test("return an empty string when passed empty string as param", () =>{
    expect(reverseString("")).toEqual("");
  });
  test("return cba when passed abc as param", () =>{
    expect(reverseString("abc")).toEqual("cba");
  });
  test("return sadida when passed adidas as param", () =>{
    expect(reverseString("adidas")).toEqual("sadida");
  });

  describe("Exepecting error", () =>{
    test("return error when passed boolean as param", () =>{
      expect(() =>{
        reverseString(true).toThrow(TypeError)
      });
    });

    test("return error when passed int as param", () =>{
      expect(() =>{
        reverseString(152).toThrow(TypeError)
      });
  })

  });

  /*
  test("return false when passed array as param", () =>{
    expect(reverseString(["abc","adidas"])).toBeFalsy();
  });
  */
})
